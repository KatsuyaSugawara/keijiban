package beans;

import java.util.Date;

public class Comments {

	private int thread_id;
	private int comment_id;
	private String text;
	private Date created_date;
	private String name;

	public void setThread_id(int thread_id){
		this.thread_id=thread_id;
	}

	public int getThread_id(){
		return thread_id;
	}

	public void setComment_id(int comment_id){
		this.comment_id=comment_id;
	}

	public int getComment_id(){
		return comment_id;
	}

	public void setText(String text){
		this.text=text;
	}

	public String getText(){
		return text;
	}

	public void setCreated_date(Date created_date){
		this.created_date=created_date;
	}

	public Date getCreated_date(){
		return created_date;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

}
