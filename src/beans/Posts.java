package beans;

import java.io.Serializable;
import java.util.Date;

public class Posts  implements Serializable{

	private static final long serialVersionUID = 1L;

	public Posts(){};

	private int thread_id;
	private String subject;
	private String text;
	private String category;
	private Date created_date;
	private String name;

	public void setThread_id(int thread_id){
		this.thread_id = thread_id;
	}

	public int getThread_id(){
		return thread_id;
	}

	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getSubject(){
		return subject;
	}

	public void setText(String text){
		this.text = text;
	}

	public String getText(){
		return text;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setCreated_date(Date created_date){
		this.created_date = created_date;
	}

	public Date getCreated_date(){
		return created_date;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}





}
