package beans;

import java.io.Serializable;

public class User implements Serializable{

	public User(){};

	private static final long serialVersionUID = 1L;

	private String login_id;
	private String password;
	private String user_name;
	private int branch_id;
	private int department_id;
	private String user_status ="正常";
	private String branch_name;
	private String department_name;

	public void setLogin_id(String login_id){
		this.login_id=login_id;
	}

	public String getLogin_id(){
		return login_id;
	}

	public void setPassword(String password){
		this.password=password;
	}

	public String getPassword(){
		return password;
	}

	public void setUser_name(String user_name){
		this.user_name=user_name;
	}

	public String getUser_name(){
		return user_name;
	}

	public int getBranch_id(){
		return branch_id;
	}

	public void setBranch_id(int branch_id){
		this.branch_id=branch_id;
	}

	public int getDepartment_id(){
		return department_id;
	}

	public void setDepartment_id(int department_id){
		this.department_id=department_id;
	}

	public String getUser_status(){
		return user_status;
	}

	public void setUser_status(String user_status){
		this.user_status=user_status;
	}

	public String getBranch_name(){
		return branch_name;
	}

	public void setBranch_name(String branch_name){
		this.branch_name=branch_name;
	}

	public String getDepartment_name(){
		return department_name;
	}

	public void setDepartment_name(String department_name){
		this.department_name=department_name;
	}

}
