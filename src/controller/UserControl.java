package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import logic.UserLogic;

@WebServlet(urlPatterns = { "/usercontrol" })
public class UserControl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session;
		session = request.getSession();
		User user = (User)session.getAttribute("loginUser");
		List<String> messages = new ArrayList<String>();
    	if(!(user.getDepartment_id()==1&&user.getBranch_id()==1)){
    		messages.add("権限が不足しています");
			session.setAttribute("comments",session.getAttribute("comments"));
			session.setAttribute("posts", session.getAttribute("posts"));
			request.setAttribute("homeerrormessages",messages);
	        request.getRequestDispatcher("top.jsp").forward(request, response);

    	}else{
    		try{
    			UserLogic selectuser = new UserLogic();
    			List<User> list = new ArrayList<User>();

    			list = selectuser.selectUser();


    			session.setAttribute("selectusers", list);

    			}catch(SQLException e){
    				throw new RuntimeException(e);
    			}
    		request.getRequestDispatcher("usercontrol.jsp").forward(request,response);
    	}



	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session =request.getSession();

		List<User> list=(List<User>)session.getAttribute("selectusers");
		String status =request.getParameter("status");

		if(status!=null){
			User user = list.get(Integer.parseInt(status));
			if(user.getUser_status().equals("正常")){
				user.setUser_status("停止中");
			}else{
				user.setUser_status("正常");
			}

			try{
				UserLogic logic = new UserLogic();
				logic.editstatus(user);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			request.getRequestDispatcher("usercontrol.jsp").forward(request,response);
		}else{
			int count = Integer.parseInt(request.getParameter("count"));
			session.setAttribute("controluser",list.get(count));
			request.getRequestDispatcher("useredit.jsp").forward(request,response);
		}
	}

}
