package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comments;
import beans.Posts;
import beans.User;
import logic.Commentlogic;
import logic.PostLogic;

@WebServlet(urlPatterns={"/comment"})
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		int id = Integer.parseInt(request.getParameter("threadid"));
		String text =request.getParameter("text");
		List<String> messages = new ArrayList<String>();


		if(text.length()>500){
			messages.add("コメントは500文字までです");
			request.setAttribute("homeerrormessages", messages);
		}

		if(text.length()==0){
			messages.add("コメントを入力してください");
			request.setAttribute("homeerrormessages", messages);
			}

		if(messages.size() != 0){

			try{
				PostLogic postlogic = new PostLogic();
				List<Posts> posts = postlogic.select();
				Commentlogic commentlogic = new Commentlogic();
				List<Comments> comments = commentlogic.select();

				request.setAttribute("comments", comments);
				request.setAttribute("posts", posts);
			}catch(SQLException e) {
				e.printStackTrace();
			}

	        request.getRequestDispatcher("top.jsp").forward(request, response);
		}else{

			HttpSession session = request.getSession();
			User user = (User)session.getAttribute("loginUser");

			Comments comment = new Comments();

			comment.setThread_id(id);
			comment.setText(text);
			comment.setName(user.getUser_name());


			try{
				Commentlogic clogic = new Commentlogic();
				clogic.insert(comment);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			response.sendRedirect("home");
		}



	}

}
