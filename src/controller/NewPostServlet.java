package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Posts;
import beans.User;
import logic.PostLogic;


@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("newpost.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

        List<String> messages = new ArrayList<String>();

		Posts posts = new Posts();
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("loginUser");

		String category = request.getParameter("category");
		String subject = request.getParameter("subject");
		String text = request.getParameter("text");

		if(StringUtils.isEmpty(category)){
			messages.add("カテゴリが未入力です");
		}

		if(StringUtils.isEmpty(subject)){
			messages.add("件名が未入力です");
		}

		if(StringUtils.isEmpty(text)){
			messages.add("本文が未入力です");
		}

		if(category.length()>10){
			messages.add("カテゴリの最大文字数は10文字です");
		}

		if(subject.length()>30){
			messages.add("件名の最大文字数は30文字です");
		}

		if(text.length()>1000){
			messages.add("本文の最大文字数は1000文字です");
		}

		if(messages.size()==0){
			posts.setCategory(request.getParameter("category"));
			posts.setSubject(request.getParameter("subject"));
			posts.setText(request.getParameter("text"));
			posts.setName(user.getUser_name());

			PostLogic pLogic = new PostLogic();

			try{
			pLogic.insert(posts);
			}catch(SQLException e) {
				e.printStackTrace();
			}
			response.sendRedirect("home");

		}else{
			request.setAttribute("posterrormessages", messages);
			request.setAttribute("postcategory", request.getParameter("category"));
			request.setAttribute("postsubject", request.getParameter("subject"));
			request.setAttribute("posttext", request.getParameter("text"));
	        request.getRequestDispatcher("newpost.jsp").forward(request, response);
		}




	}
}
