package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Comments;
import beans.Posts;
import logic.Commentlogic;
import logic.PostLogic;

@WebServlet(urlPatterns={"/home"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try{
			PostLogic postlogic = new PostLogic();
			List<Posts> posts = postlogic.select();
			Commentlogic commentlogic = new Commentlogic();
			List<Comments> comments = commentlogic.select();

			session.setAttribute("comments", comments);
			session.setAttribute("posts", posts);
		}catch(SQLException e) {
			e.printStackTrace();
		}

        request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String startdate = request.getParameter("startdate");
		String enddate = request.getParameter("enddate");
		String search = request.getParameter("search");
		String delpost = request.getParameter("delpost");
		String commentthreadid = request.getParameter("delcommentthreadid");
		String commentid = request.getParameter("delcommentid");

		if(!(delpost==null)){
			try{
				PostLogic post = new PostLogic();
				post.delete(delpost);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			response.sendRedirect("home");

		}else if(!(commentid==null)){
			try{
				Commentlogic comment = new Commentlogic();
				comment.delete(commentthreadid,commentid);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			response.sendRedirect("home");

		}else{


			if(startdate==""){
				startdate ="2000-01-01";
			}

			if(enddate==""){
				enddate ="2999-01-01";
			}

			try{
				PostLogic postlogic = new PostLogic();
				List<Posts> posts = postlogic.select(startdate,enddate,search);
				Commentlogic commentlogic = new Commentlogic();
				List<Comments> comments = commentlogic.select();

				request.setAttribute("comments", comments);
				request.setAttribute("posts", posts);
				request.getRequestDispatcher("top.jsp").forward(request, response);
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
