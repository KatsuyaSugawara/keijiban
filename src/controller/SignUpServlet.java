package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import logic.UserLogic;

@WebServlet(urlPatterns = { "/userregster" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	//ログインユーザー情報取得
		HttpSession session = request.getSession();
    	User user = (User)session.getAttribute("loginUser");

    	List<String> messages = new ArrayList<String>();


    	if(!(user.getDepartment_id()==1&&user.getBranch_id()==1)){
    		messages.add("権限が不足しています");
			request.setAttribute("errorMessages",messages);
	        request.getRequestDispatcher("top.jsp").forward(request, response);

    	}else{
	        request.getRequestDispatcher("signup.jsp").forward(request, response);
    	}


    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		request.setCharacterEncoding("UTF-8");
        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setUser_name(request.getParameter("name"));
            user.setBranch_id(Integer.parseInt(request.getParameter("branch")));
            user.setDepartment_id(Integer.parseInt(request.getParameter("departments")));


            try {
				new UserLogic().register(user);
			} catch (SQLException e) {
				e.printStackTrace();
			}

            response.sendRedirect("home");
        } else {
            request.setAttribute("signuperrorMessages", messages);
	        request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

    	String name = request.getParameter("name");
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        String checkpassword = request.getParameter("checkpassword");

        if (StringUtils.isEmpty(name) == true) {
            messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(account) == true) {
            messages.add("アカウント名を入力してください");
        }

        if(!(Pattern.matches("^[0-9a-zA-Z]+$", account))){
        	messages.add("アカウント名の登録は半角英数字のみです");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if(!(password.equals(checkpassword))){
        	messages.add("パスワードが一致しません");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}