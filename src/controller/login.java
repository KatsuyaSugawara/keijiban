package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import logic.LoginLogic;

/**
 * Servlet implementation class login
 */

@WebServlet(urlPatterns = {"/log"})
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		try {
			String account = request.getParameter("account");
			String password = request.getParameter("password");

			LoginLogic loginLogic = new LoginLogic();

			User user = loginLogic.login(account, password);
			HttpSession session = request.getSession();

			if(user != null && user.getUser_status().equals("正常")){

				session.setAttribute("loginUser", user);
				session.setAttribute("isShowMessageForm", true);
				response.sendRedirect("home");

			}else{

				List<String> messages = new ArrayList<String>();
				messages.add("ログインに失敗しました。");
				session.setAttribute("errorMessages",messages);
				response.sendRedirect("login");
			}

		} catch (SQLException e) {
			System.out.println("login " + e);
		}
	}

}
