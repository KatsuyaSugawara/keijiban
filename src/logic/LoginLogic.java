package logic;


import static util.DBUtil.*;
import java.sql.Connection;
import java.sql.SQLException;

import beans.User;
import dao.UserDao;
import util.CipherUtil;

public class LoginLogic {

	public User login(String account,String password) throws SQLException{

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getUser(connection, account, encPassword);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
        	connection.close();
        }
	}

}
