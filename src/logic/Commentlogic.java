package logic;

import static util.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Comments;
import dao.CommentsDao;

public class Commentlogic {
	   public void insert(Comments comments) throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            CommentsDao dao = new CommentsDao();
	            dao.insert(connection, comments);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }

	   public List<Comments> select() throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            CommentsDao dao = new CommentsDao();
	            List<Comments> commentlist= dao.getComments(connection);
	            commit(connection);
	            return commentlist;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }

	   public void delete(String threadid,String commentid) throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            CommentsDao dao = new CommentsDao();
	            dao.delete(connection,threadid,commentid);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }
}
