package logic;

import static util.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import beans.Posts;
import dao.PostsDao;

public class PostLogic {
	   public void insert(Posts posts) throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            PostsDao dao = new PostsDao();
	            dao.insert(connection, posts);

	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }
	   public List<Posts> select() throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();
	            String category = "";
	            String day1 = "2000-01-01";
	            String day2 = "2999-01-01";


	            PostsDao dao = new PostsDao();
	            List<Posts> postlist= dao.getPost(connection,category,day1,day2);
	            commit(connection);
	            return postlist;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }

	   public List<Posts> select(String startday,String endday,String search) throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();
	            String category = search;
	            String day1 = startday;
	            String day2 = endday;


	            PostsDao dao = new PostsDao();
	            List<Posts> postlist= dao.getPost(connection,category,day1,day2);
	            commit(connection);
	            return postlist;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }

	   public void delete(String threadid) throws SQLException {

	        Connection connection = null;
	        try {
	            connection = getConnection();


	            PostsDao dao = new PostsDao();
	            dao.delete(connection,threadid);
	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            connection.close();;
	        }
	    }
}
