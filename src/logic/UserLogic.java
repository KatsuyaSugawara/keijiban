package logic;

import static util.DBUtil.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import dao.UserDao;
import util.CipherUtil;

public class UserLogic {

    public void register(User user) throws SQLException {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            connection.close();;
        }
    }

    public void edit(User user) throws SQLException {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.update(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            connection.close();;
        }
    }

    public void editstatus(User user) throws SQLException {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.updatestatus(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            connection.close();;
        }
    }

    public List<User> selectUser() throws SQLException{

    	Connection connection=null;
    	try{
    		connection=getConnection();

    		UserDao userDao = new UserDao();

    		List<User> userList = new ArrayList<User>();
    		userList = userDao.selectAllUser(connection);
    		return userList;


    	}catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            connection.close();;
        }

    }
}

