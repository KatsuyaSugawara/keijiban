package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import beans.Comments;

public class CommentsDao {
    public void insert(Connection connection, Comments comments) throws SQLException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("thread_id");
            sql.append(",text");
            sql.append(",created_date");
            sql.append(",name");
            sql.append(",comment_id");
            sql.append(") SELECT ");
            sql.append(" ?");
            sql.append(",?");
            sql.append(",CURRENT_TIMESTAMP");
            sql.append(", ?");
            sql.append(", CASE WHEN MAX(comment_id) IS NULL THEN 1 ELSE MAX(comment_id)+1 END FROM comments");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comments.getThread_id());
            ps.setString(2, comments.getText());
            ps.setString(3, comments.getName());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ps.close();
        }
    }

	public List<Comments> getComments(Connection connection) throws SQLException{

		PreparedStatement ps = null;

		try{

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * FROM  comments order by thread_id,comment_id");

			ps = connection.prepareStatement(sql.toString());

	        ResultSet rs = ps.executeQuery();
	        List<Comments> CommentsList = toCommentsList(rs);

	            return CommentsList;

		}catch(SQLException e){
			throw new RuntimeException(e);
		}finally{
			ps.close();
		}
	}
	   private List<Comments> toCommentsList(ResultSet rs) throws SQLException {

	        List<Comments> ret = new ArrayList<Comments>();
	        try {
	            while (rs.next()) {

	                int thread_id = rs.getInt("thread_id");
	                int comment_id = rs.getInt("comment_id");
	                String text = rs.getString("text");
	                String created_date = rs.getString("created_date");
	                String name = rs.getString("name");


	                Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(created_date);

	                Comments comments= new Comments();

	    			comments.setThread_id(thread_id);
	    			comments.setComment_id(comment_id );
	    			comments.setText(text);
	    			comments.setCreated_date(date);
	    			comments.setName(name);

	                ret.add(comments);
	            }
	            return ret;
	        }catch (ParseException ex) {
    			ex.printStackTrace();
    			return ret;
    		} finally {
	            rs.close();
	        }
	    }
	    public void delete(Connection connection, String threadid,String commentid) throws SQLException {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("DELETE FROM comments WHERE ");
	            sql.append("thread_id = ");
	            sql.append("?");
	            sql.append(" and ");
	            sql.append("comment_id = ");
	            sql.append("?");


	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, threadid);
	            ps.setString(2, commentid);

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new RuntimeException(e);
	        } finally {
	            ps.close();
	        }
	    }
}
