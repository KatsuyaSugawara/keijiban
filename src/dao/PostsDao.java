package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Posts;

public class PostsDao {

    public void delete(Connection connection, String threadid) throws SQLException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM posts WHERE ");
            sql.append("thread_id =");
            sql.append("?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1,threadid);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            ps.close();
        }
    }
	    public void insert(Connection connection, Posts posts) throws SQLException {

	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("INSERT INTO posts ( ");
	            sql.append("subject");
	            sql.append(", text");
	            sql.append(", category");
	            sql.append(", created_date");
	            sql.append(", name");
	            sql.append(", thread_id");
	            sql.append(") SELECT ");
	            sql.append(" ?");
	            sql.append(", ?");
	            sql.append(", ?");
	            sql.append(", CURRENT_TIMESTAMP");
	            sql.append(", ?");
	            sql.append(", CASE WHEN MAX(thread_id) IS NULL THEN 1 ELSE MAX(thread_id)+1 END FROM posts");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setString(1, posts.getSubject());
	            ps.setString(2, posts.getText());
	            ps.setString(3, posts.getCategory());
	            ps.setString(4, posts.getName());

	            ps.executeUpdate();
	        } catch (SQLException e) {
	            throw new RuntimeException(e);
	        } finally {
	            ps.close();
	        }
	    }

		public List<Posts> getPost(Connection connection,String category,String day1,String day2) throws SQLException{

			PreparedStatement ps = null;

			try{
				Date startday = Date.valueOf(day1);
				Date endday = Date.valueOf(day2);

	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT * FROM  posts ");
	            sql.append("WHERE created_date between");
	            sql.append("?");
	            sql.append("and");
	            sql.append("?");
	            sql.append("and category like ");
	            sql.append("?");
				ps = connection.prepareStatement(sql.toString());

				ps.setDate(1,startday);
				ps.setDate(2,endday);
				ps.setString(3,"%"+category+"%");
		        ResultSet rs = ps.executeQuery();
		        List<Posts> postsList = toPostsList(rs);

		            return postsList;

			}catch(SQLException e){
				throw new RuntimeException(e);
			}finally{
				ps.close();
			}
		}
		   private List<Posts> toPostsList(ResultSet rs) throws SQLException {

		        List<Posts> ret = new ArrayList<Posts>();
		        try {
		            while (rs.next()) {

		                int thread_id = rs.getInt("thread_id");
		                String subject = rs.getString("subject");
		                String text = rs.getString("text");
		                String category = rs.getString("category");
		                Date created_date = rs.getDate("created_date");
		                String name = rs.getString("name");

		                Posts posts= new Posts();

		    			posts.setThread_id(thread_id);
		    			posts.setSubject(subject);
		    			posts.setText(text);
		    			posts.setCategory(category);
		    			posts.setCreated_date(created_date);
		    			posts.setName(name);

		                ret.add(posts);
		            }
		            return ret;
		        }finally {
		            rs.close();
		        }
		    }

}
