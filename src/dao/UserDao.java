package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;

public class UserDao {

	public User getUser(Connection connection,String user_id,String password) throws SQLException{

		PreparedStatement ps = null;

		try{
			String sql="SELECT * from users join branch on users.branch_id=branch.branch_id join departments on users.department_id=departments.department_id WHERE login_id=? and password=?";

			ps = connection.prepareStatement(sql);
			ps.setString(1,user_id);
			ps.setString(2,password);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }

		}catch(SQLException e){
			throw new RuntimeException(e);
		}finally{
			ps.close();
		}
	}

	public List<User> selectAllUser(Connection connection) throws SQLException{

		PreparedStatement ps = null;

		try{
			String sql="select * from users join branch on users.branch_id=branch.branch_id join departments on users.department_id=departments.department_id";

			ps = connection.prepareStatement(sql);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);

	        return userList;

		}catch(SQLException e){
			throw new RuntimeException(e);
		}finally{
			ps.close();
		}
	}

    public void insert(Connection connection, User user) throws SQLException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch_id");
            sql.append(", department_id");
            sql.append(", user_status");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(", ?");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUser_name());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
            ps.setString(6, user.getUser_status());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
        	ps.close();
        }
    }

    public void update(Connection connection, User user) throws SQLException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("login_id=");
            sql.append("?");
            sql.append(", password=");
            sql.append("?");
            sql.append(", user_name=");
            sql.append("?");
            sql.append(", branch_id=");
            sql.append("?");
            sql.append(", department_id=");
            sql.append("?");
            sql.append(", user_status=");
            sql.append("?");
            sql.append("WHERE login_id=");
            sql.append("?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUser_name());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getDepartment_id());
            ps.setString(6, user.getUser_status());
            ps.setString(7, user.getLogin_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
        	ps.close();
        }
    }

    public void updatestatus(Connection connection, User user) throws SQLException {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("user_status=");
            sql.append("?");
            sql.append("WHERE login_id=");
            sql.append("?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getUser_status());
            ps.setString(2, user.getLogin_id());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
        	ps.close();
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {

                String account = rs.getString("login_id");
                String password = rs.getString("password");
                String name = rs.getString("user_name");
                int branch = rs.getInt("branch_id");
                int departments = rs.getInt("department_id");
                String status = rs.getString("user_status");
                String branchname =rs.getString("branch_name");
                String departmentname=rs.getString("department_name");


                User user = new User();

    			user.setLogin_id(account);
    			user.setPassword(password);
    			user.setUser_name(name);
    			user.setBranch_id(branch);
    			user.setDepartment_id(departments);
    			user.setUser_status(status);
    			user.setBranch_name(branchname);
    			user.setDepartment_name(departmentname);

                ret.add(user);
            }
            return ret;
        } finally {
            rs.close();
        }
    }

}
