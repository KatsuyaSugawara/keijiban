<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
<link href="./css/background.css" rel="stylesheet" type="text/css">
<link href="./css/usercontrol.css" rel="stylesheet" type="text/css">
<script src="./js/home.js"></script>
</head>
<body>
	<a href="userregster">ユーザー登録</a>
	<a href="./home">戻る</a>
<table>
	<tr>
		<th>ログインID</th>
		<th>名前</th>
		<th>支店</th>
		<th>役職</th>
		<th>ステータス</th>
		<th>編集</th>
	</tr>
	<c:forEach var="user" items="${selectusers}" varStatus="i" begin="0">
	<tr>
		<td>${user.login_id }</td>
		<td>${user.user_name }</td>
		<td>${user.branch_name }</td>
		<td>${user.department_name }</td>
		<td>
			<form name ="form1${i.index }"action="usercontrol" method="post" onSubmit="return check()">
				<input type="hidden" name="status" value="${i.index }">
				<input type="submit"  value="${user.user_status }" />
			</form>
		</td>
		<td>
			<form name ="form${i.index }"action="usercontrol" method="post">
			<input type="hidden" name="count" value="${i.index }">
			<input type="submit"  value="編集"/>
			</form>
		</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>