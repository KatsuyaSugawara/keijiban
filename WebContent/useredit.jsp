<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>ユーザー編集</title>
    	<link href="./css/background.css" rel="stylesheet" type="text/css">
    </head>
    <c:if test="${ not empty editerrorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${editerrorMessages}" var="errormessage">
					<li>
						<c:out value="${errormessage}" />
					</li>
                </c:forEach>
            </ul>
       </div>
	</c:if>
    <body>
        <div class="main-contents">
            <form action="useredit" method="post"><br />
			<label for="name">名前</label><br/>
			<input name="name" id="name" value="${controluser.user_name }"/><br />
			<label for="account">アカウント名</label><br />
			<input name="account" id="account" value="${controluser.login_id }"/> <br />
			<label for="password">パスワード</label><br />
			<input name="password" type="password" id="password" /> <br />
			<label for="checkpassword">もう一度パスワードを入力してください</label><br />
			<input name="checkpassword" type="password" id="checkpassword" /> <br />
			<label for="branch">支店</label>
			<select name ="branch" id ="branch">
				<option value=1>本社</option>
				<option value=2>福岡</option>
			</select><br />
			<label for="departments">部署</label>
			<select name="departments" id="departments">
				<option value=1>総務部</option>
				<option value=2>企画部</option>
			</select><br />
			<input type="submit" value="登録" /> <br /> <a href="./home">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
		</div>
    </body>
</html>