<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>掲示板ホーム</title>
		<link href="./css/home.css" rel="stylesheet" type="text/css">
		<link href="./css/background.css" rel="stylesheet" type="text/css">
		<script src="./js/home.js"></script>
	</head>
	<body>
	<c:if test="${ not empty homeerrormessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${ homeerrormessages}" var="errormessage">
					<li>
						<c:out value="${errormessage}" />
					</li>
                </c:forEach>
            </ul>
       </div>
	</c:if>
		<div class="main-contents">
			<div class="header">
				<c:if test="${ not empty loginUser }">
					<a href="newpost">新規投稿</a>
					<a href="usercontrol">ユーザー管理</a>
					<a href="logout">ログアウト</a>
				</c:if>
			</div>
			<div class="searchmenu">
				<form action="home" method="post">
					<label>開始日:<input type="date" name="startdate"></label> <br />
					<label>終了日:<input type="date" name="enddate"></label> <br />
					<label for="search">検索</label>
					<input name="search" id="search"/>
					<input type="submit" value="検索"><br />
				</form>
			</div>
			<div class="postsmenu">
				<c:forEach var="varposts" items="${posts}" varStatus="i" begin="0">
					<div class="posdel">
					<c:if test="${loginUser.user_name == varposts.name}">
						<form action="home" method="post">
						    <input type="hidden"  name='delpost'value="${varposts.thread_id}">
							<input type="submit" value="投稿削除"/> <br />
						</form>
					</c:if>
					</div>
						<div class ="postborder">
						<div class="title">
							<c:out value="${varposts.getSubject()}"/> <br />
							<c:out value="name:${varposts.name}"/>
							<c:out value="${varposts.created_date}"/>
						</div>
						<div class="mainpost">
						<div class ="category">
						<c:out value=" カテゴリ：${varposts.category}"/> <br />
						</div>
						<div class="posttext">
<c:out value="${varposts.text}"/> <br />
						</div>
						<c:forEach var="varcomment" items="${comments}" varStatus="i" begin="0">
						<div class ="commentlist">
							<c:if test="${varcomment.thread_id == varposts.thread_id}">
							<c:out value="${varcomment.text}"/> <br />
							<div class="commentname">
							<c:out value="name:${varcomment.name}"/>
							<c:out value="${varcomment.created_date}"/>
							</div>
							<c:if test="${loginUser.user_name == varcomment.name}">
							<form action="home" method="post">
							<input type="hidden" name="delcommentthreadid" value="${varposts.thread_id}">
							<input type="hidden" name="delcommentid" value="${varcomment.comment_id}">
							<input type="submit" value="コメント削除" name='delcommentid'/> <br />
							</form>
							</c:if>
							</c:if>
						</div>
						</c:forEach>
						<div class="tocomment">
						<form action="comment" method="post">
							<label for="text">コメントする</label><br />
							<textarea name="text" rows="5" cols="50"></textarea><br />
							<input type="hidden" name="threadid" value="${varposts.thread_id}">
							<input type="submit" value="送信" /> <br />
						</form>
						</div>
						</div>
					</div>
					<br /><br />
				</c:forEach>
			</div>
		</div>
	</body>
</html>