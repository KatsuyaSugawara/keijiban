<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
<link href="./css/background.css" rel="stylesheet" type="text/css">
</head>
<body>
    <c:if test="${ not empty posterrormessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${posterrormessages}" var="errormessage">
					<li>
						<c:out value="${errormessage}" />
					</li>
                </c:forEach>
            </ul>
       </div>
	</c:if>
	<a href="./home">ホームへ戻る</a>
	<form action="newpost" method="post"><br />
		<label for="category">カテゴリ</label>
		<input name="category" id="category" value="${postcategory}" /><br />
		<label for="subject">件名</label>
		<input name="subject" id="subject" value="${postsubject}"/><br />
		<label for="text">本文</label><br />
		<textarea name="text" rows="10" cols="100">${posttext}</textarea><br />
		<input type="submit" value="送信" /> <br />
	</form>
</body>
</html>